import pkg from 'gulp';
import gulpSass from 'gulp-sass';
import * as dartSass from 'sass';
import fileInclude from 'gulp-file-include';
import browserSync from 'browser-sync';
import clean from 'gulp-clean';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import purgecss from 'gulp-purgecss';
import minifyjs from 'gulp-js-minify';
import imagemin from 'gulp-imagemin';
import concat from 'gulp-concat';


const sass = gulpSass(dartSass);
const {task, watch, series, src, dest, parallel} = pkg;


task('styleCSS', () => {
    return src("./src/styles/style.scss")
        .pipe(sass.sync({
            sourceComments: false,
            outputStyle: "expanded"
        }).on('error', sass.logError))
        .pipe(purgecss({
            content: ['src/**/*.html', './src/js/**/*.js']
        }))
        .pipe(autoprefixer({ overrideBrowserslist: ['last 5 versions'] }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('styles.min.css'))
        .pipe(dest("./dist/styles/"))
        .pipe(browserSync.stream());
})

task('moveHTML', () => {
    return src("./src/*.html")
        .pipe(fileInclude())
        .pipe(dest("./dist"))
        .pipe(browserSync.stream());
})

task('moveIMG', () => {
    return src("./src/images/**/*.*")
        .pipe(imagemin())
        .pipe(dest("./dist/images/"));
})

task('moveJS', () => {
    return src("./src/js/**/*.js")
        .pipe(minifyjs())
        .pipe(concat('scripts.min.js'))
        .pipe(dest("./dist/js/"));
})


task('clean', () => {
    return src("./dist/*")
        .pipe(clean());
})

task('serve', () => {
    return browserSync.init({
        server: {
            baseDir: ['dist']
        },
        port: 9000,
        open: true
    });
});
task('watchers', () => {
    watch('./src/styles/**/*.scss', parallel('styleCSS')).on('change', browserSync.reload);
    watch('./src/views/*.html', parallel('moveHTML')).on('change', browserSync.reload);
    watch('./src/images/**/*.*', parallel('moveIMG')).on('change', browserSync.reload);
    watch('./src/js/**/*.js', parallel('moveJS')).on('change', browserSync.reload);
});

task('build', series('clean', 'styleCSS', 'moveJS', 'moveIMG', 'moveHTML'));

task('dev', series('build', parallel('serve', 'watchers')));
