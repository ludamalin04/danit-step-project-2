let btn = document.querySelector('.header__burger');
let menu = document.querySelector('.header__drop-down');

document.addEventListener('click', function (event) {
    let element = event.target;
    if (menu.classList.contains('visible')) {
        if (element !== menu && element !== btn && !element.matches('.header__link')) {
            menu.classList.remove('visible');
            btn.classList.toggle('active');
        }
    }
    if (element === btn){
        btn.classList.toggle('active');
        menu.classList.toggle('visible');
    }
})
